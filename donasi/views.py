from django.shortcuts import render,redirect
from django.db.models import Sum
from .models import Donasi
from .forms import DonasiForm

# Create your views here.
def donasi(request):
    all_donasi = Donasi.objects.all().order_by('-id')[:5]
    jumlah = Donasi.objects.aggregate(Sum('jumlah'))
    context = {
        'all_donasi':all_donasi,
        'jumlah':jumlah
    }
    return render(request,'donasi.html',context)

def createDonasi(request):
    donasi_form = DonasiForm(request.POST or None)
    if request.method == 'POST' :
        if donasi_form.is_valid():
            donasi_form.save()
        return redirect('donasi:donasi')

    context = {
        "page_title":"Silahkan Berdonasi",
        "donasi_form":donasi_form,
    }
    return render(request,'create.html',context)

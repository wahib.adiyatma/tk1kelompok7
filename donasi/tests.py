from django.test import TestCase, Client
from django.urls import resolve
from .models import Donasi
from .forms import DonasiForm

# Create your tests here.

class DonasiUnitTest(TestCase):
    def test_url(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code,200)
    
    def test_template(self):
        response = Client().get('/donasi/')
        self.assertTemplateUsed(response,'donasi.html')
    
    def test_create_model(self):
        Donasi.objects.create(nama="INI NAMA", pesan_singkat="INI PESAN",jumlah="100")
        count = Donasi.objects.all().count()
        self.assertEquals(count, 1)

    def test_url_form(self):
        response = Client().get('/donasi/covid/')
        self.assertEquals(response.status_code, 200)
    
    def test_post_form(self):
        response = Client().post("/donasi/covid/", {"nama":"INI NAMA","pesan_singkat":"INI PESAN","jumlah":"100"})
        self.assertEqual(response["Location"], "/donasi/")
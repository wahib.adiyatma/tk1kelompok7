# Generated by Django 3.1.3 on 2020-11-10 05:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_auto_20201109_1821'),
    ]

    operations = [
        migrations.CreateModel(
            name='DataProvinsi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=100)),
                ('confirmed', models.CharField(max_length=100)),
                ('recovered', models.CharField(max_length=100)),
                ('death', models.CharField(max_length=100)),
            ],
        ),
    ]

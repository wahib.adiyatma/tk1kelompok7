from django.shortcuts import render
import requests
from django.utils import timezone
from .models import DataCovidIndonesia, DataProvinsi


def home_views(request, *args, **kwargs):
    timezone.activate("Asia/Jakarta")


    data = {
        "dataKasus" : None,
        "allProv" : None,
        "userProv":"DKI Jakarta",
        "dataProv" : None,
        "tanggal" : timezone.localdate(),  
    }
    
    if not DataProvinsi.objects.all().exists() :
        resp = requests.get("https://api.kawalcorona.com/indonesia/provinsi/")
        provjson = resp.json()

        for prov in provjson :
            angkaConfirmed = ubah_format_angka(prov["attributes"]["Kasus_Posi"])
            angkaRecovered = ubah_format_angka(prov["attributes"]["Kasus_Semb"])
            angkaDeath = ubah_format_angka(prov["attributes"]["Kasus_Meni"])
            provcovid = DataProvinsi.objects.create(nama=prov["attributes"]["Provinsi"], 
            confirmed=angkaConfirmed,
            recovered=angkaRecovered,
            death= angkaDeath)
        
    if DataCovidIndonesia.objects.filter(date=timezone.localdate()).exists() :
        data["dataKasus"] = DataCovidIndonesia.objects.get(date=timezone.localdate())
        data["dataProv"] = DataProvinsi.objects.get(nama="DKI Jakarta")
        
    else:
        respIndo = requests.get("https://api.kawalcorona.com/indonesia/")
        respProv = requests.get("https://api.kawalcorona.com/indonesia/provinsi/")
        dataJson = respIndo.json()
        newData = DataCovidIndonesia.objects.create(date=timezone.localdate(), confirmed=dataJson[0]["positif"], recovered=dataJson[0]["sembuh"], death=dataJson[0]["meninggal"])
        data["dataKasus"] = newData
        provjson = respProv.json()
        for prov in provjson :
            angkaConfirmed = ubah_format_angka(prov["attributes"]["Kasus_Posi"])
            angkaRecovered = ubah_format_angka(prov["attributes"]["Kasus_Semb"])
            angkaDeath = ubah_format_angka(prov["attributes"]["Kasus_Meni"])
            provcovid = DataProvinsi.objects.get(nama=prov["attributes"]["Provinsi"])
            provcovid.confirmed = angkaConfirmed
            provcovid.recovered = angkaRecovered
            provcovid.death = angkaDeath
        data["dataProv"] = DataProvinsi.objects.get(nama="DKI Jakarta")

    if request.method == "POST" :
        data["userProv"] = request.POST.get("provinsi")
        data["dataProv"] = DataProvinsi.objects.get(nama=data["userProv"])

    data["allProv"] = DataProvinsi.objects.all()
    return render(request, "home.html", data)

def ubah_format_angka(angka):
    angkaInt = int(angka)
    formatedAngka = f"{angkaInt:,}"
    return formatedAngka


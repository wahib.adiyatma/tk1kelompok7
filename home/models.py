from django.db import models
import datetime

class DataCovidIndonesia(models.Model):
    date = models.DateField()
    confirmed = models.CharField(max_length = 100)
    recovered = models.CharField(max_length = 100)
    death = models.CharField(max_length = 100)
    
class DataProvinsi(models.Model):
    nama = models.CharField(max_length = 100)
    confirmed = models.CharField(max_length = 100)
    recovered = models.CharField(max_length = 100)
    death = models.CharField(max_length = 100)

    class Meta:
        ordering = ['nama']

    def __str__(self):
        return self.nama
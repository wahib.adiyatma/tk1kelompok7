from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index_dataprov, name='index_dataprov'),
    path('hasil_cari', views.hasil_cari, name='hasil_cari'),
    ]

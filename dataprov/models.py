from django.db import models

class SearchBar(models.Model):
	search = models.CharField(max_length=50)
	def __str__(self):
		return self.search


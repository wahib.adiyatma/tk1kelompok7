from django.test import TestCase, Client

class test_templates(TestCase):

	def test_tampilan_indexDataprov(self):
		response = Client().get('/dataprov/')
		isi = response.content.decode('utf8')
		self.assertIn('<form method="POST">',isi)
		self.assertIn('Cari Data',isi)
	
	def test_tampilan_indexDataprov(self):
		response = Client().get('/dataprov/hasil_cari')
		isi = response.content.decode('utf8')
		self.assertIn('<form method="POST">',isi)
		self.assertIn('Cari Data',isi)
		self.assertIn('DATA STATISTIK COVID-19 PROVINSI', isi)
		self.assertIn('Total Kasus Terkonfirmasi :', isi)
		self.assertIn('Pasien Sembuh', isi)
		self.assertIn('Pasien Dirawat', isi)
		self.assertIn('Pasien Meninggal', isi)
		self.assertIn('Penambahan Sembuh', isi)
		self.assertIn('Penambahan Positif', isi)
		self.assertIn('Penambahan Meninggal', isi)
		self.assertIn('id="myChart"', isi)
		self.assertIn('id="myChart2"', isi)


		




		



	
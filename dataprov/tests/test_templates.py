from django.test import TestCase, Client

class test_templates(TestCase):

	def test_index(self):
		response = Client().get('/dataprov/')
		self.assertTemplateUsed(response, 'dataprov/index.html')
		self.assertTemplateUsed(response, 'base.html')
		self.assertTemplateUsed(response, 'snippets/navbar.html')
		self.assertTemplateUsed(response, 'snippets/footer.html')

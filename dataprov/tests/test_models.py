from django.test import TestCase, Client
from dataprov.models import *



class test_searchBar(TestCase):

	def test_hitung_search(self):
		SearchBar.objects.create(search = "DKI JAKARTA")
		count = SearchBar.objects.filter(search = "DKI JAKARTA").count()
		self.assertEquals(count, 1)

	def test_search(self):
		prov = SearchBar.objects.create(search = "DKI JAKARTA")
		self.assertEquals(prov.search, "DKI JAKARTA")
		prov = SearchBar.objects.create(search = "JAWA BARAT")
		self.assertEquals(prov.search, "JAWA BARAT")
		prov = SearchBar.objects.create(search = "JAWA TIMUR")
		self.assertEquals(prov.search, "JAWA TIMUR")
		prov = SearchBar.objects.create(search = "JAWA TENGAH")
		self.assertEquals(prov.search, "JAWA TENGAH")
		prov = SearchBar.objects.create(search = "JAMBI")
		self.assertEquals(prov.search, "JAMBI")
		prov = SearchBar.objects.create(search = "KALIMANTAN TIMUR")
		self.assertEquals(prov.search, "KALIMANTAN TIMUR")
		prov = SearchBar.objects.create(search = "SUMATERA UTARA")
		self.assertEquals(prov.search, "SUMATERA UTARA")






from django.shortcuts import render, redirect
import requests
from django.utils import timezone
from .forms import SearchBarForm
from .models import SearchBar
from . import dataprov_api

def index_dataprov(request):
	search_form = SearchBarForm(request.POST or None)
	isi_data = dataprov_api.isi_data


	if request.method == 'POST':
		if search_form.is_valid():
			search = request.POST.get('search').upper()
			for data_prov in isi_data:
				if(search == data_prov['key']):
					prov = search
					jumlah_kasus = data_prov['jumlah_kasus']
					jumlah_dirawat = data_prov['jumlah_dirawat']
					jumlah_sembuh = data_prov['jumlah_sembuh']
					jumlah_meninggal = data_prov['jumlah_meninggal']
					penambahan_positif = data_prov['penambahan']['positif']
					penambahan_sembuh = data_prov['penambahan']['sembuh']
					penambahan_meninggal = data_prov['penambahan']['meninggal']
					context={
						'isi_data' : isi_data,
						'search_form' : search_form,
						'prov' : prov,
						'jumlah_kasus' : jumlah_kasus,
						'jumlah_sembuh' : jumlah_sembuh,
						'jumlah_meninggal': jumlah_meninggal,
						'jumlah_dirawat' : jumlah_dirawat,
						'penambahan_positif' : penambahan_positif,
						'penambahan_meninggal' : penambahan_meninggal,
						'penambahan_sembuh' : penambahan_sembuh,
					}
					response = hasil_cari(request, context)
					return response

	context={
		'isi_data' : isi_data,
		'search_form' : search_form,
	}

	return render(request, 'dataprov/index.html', context);

def hasil_cari(request, newContext={}):
	search_form = SearchBarForm(request.POST or None)
	isi_data = dataprov_api.isi_data
	context={
		'isi_data' : isi_data,
		'search_form' : search_form,
	}
	context.update(newContext)

	return render(request, 'dataprov/hasil_cari.html', context);

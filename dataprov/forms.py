from django import forms
from .models import SearchBar

class SearchBarForm(forms.ModelForm):
	class Meta :
		model = SearchBar
		fields = [
			'search',
		]

		widgets = {
		'search' : forms.TextInput(
				attrs={
					'class':'form-control',
					'style' : 'border-radius:15px;',
					'placeholder':'Pantau Data Covid-19 Berdasarkan Provinsi'
				}
			),
		}

# TK1Kelompok7

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

## Anggota
- Rheznandya Erwanto (1906318924)
- Wahib Adiyatma (1906350780)
- Ilma Alpha Mannix (1906351045)
- Fahmi Firstian Dani (1906353643)

Tugas ini telah ter-deploy di Heroku dengan [URL](https://coronappw7.herokuapp.com/).

## Deskripsi Aplikasi
Aplikasi yang akan kami buat adalah aplikasi info-info terkait Covid19. Dimulai dari jumlah data orang yang sudah  terinfeksi corona sampai yang sudah sembuh. Selain itu, di aplikasi ini juga dapat mencari jumlah kasus di provinsi. Selain info-info tersebut, terdapat juga penggalangan donasi bagi orang-orang yang terdampak covid secara finansial. Yang terakhir, tidak kalah pentingnya, yaitu terdapat pusat bantuan maupun lokasi rumah sakit terdekat yang memudahkan kalian dalam mencari bantuan. Manfaat website yang kami buat adalah memudahkan berbagai kalangan dalam mencari informasi terkait covid19 dan berbagi kebaikan terhadap sesama. Stay safe everyone!

## Daftar Fitur
1. Homepage
- Lokasi
- Counter with date

2. Data per provinsi
- Counter per provinsi
- Searching

3. Penggalangan Donasi Covid 19
- Formulir penggalangan
- Menampikan jumlah penggalangan dana
- Menampilkan 5 data terakhir penggalang

4. Pusat bantuan
- Hotline
- Rumah sakit rujukan sesuai lokasi


## Lisensi

Templat ini didistribusikan dengan lisensi [The Unlicense][license]. Proyek
yang dibuat dengan templat ini dipersilakan untuk didistribusikan dengan
ketentuan yang berbeda.

[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/wahib.adiyatma/tk1kelompok7/-/commits/master
[license]: LICENSE
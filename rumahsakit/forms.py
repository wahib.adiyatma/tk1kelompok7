from .models import Rumahsakit
from django import forms

class RumahsakitForm(forms.ModelForm):
    class Meta:
        model = Rumahsakit
        fields = '__all__'

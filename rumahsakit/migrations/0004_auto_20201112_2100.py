# Generated by Django 3.1.3 on 2020-11-12 14:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rumahsakit', '0003_auto_20201112_2056'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rumahsakit',
            name='lokasi',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='nama',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='notelp',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='waktu',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='rumahsakit',
            name='website',
            field=models.CharField(max_length=200, null=True),
        ),
    ]

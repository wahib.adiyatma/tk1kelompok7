from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *

def rumahsakit(request):
    rumahsakit = Rumahsakit.objects.all()

    context = {
        'data' : rumahsakit
    }
    return render(request, 'template/rumahsakit.html', context)

def tambah(request):
    jenis = request.POST.get('jenis', None)
    nama = request.POST.get('nama', None)
    lokasi = request.POST.get('lokasi',None)
    waktu = request.POST.get('waktu', None)
    notelp = request.POST.get('notelp', None)
    website = request.POST.get('website', None)
    maps = request.POST.get('maps', None)
    foto = request.POST.get('foto', None)

    Rumahsakit.objects.create(jenis = jenis, nama = nama, lokasi = lokasi, waktu = waktu, notelp = notelp, website = website, maps = maps, link_foto = foto)
    return redirect('/rumahsakit')

def hospital(request):
    jenis_tempat = Rumahsakit.objects.filter(jenis = 'Rumah Sakit') #Kalo error ganti jadi one
    jumlah = jenis_tempat.count()
    
    context = {
        'data': jenis_tempat,
        'jenis': 'Rumah Sakit',
        'jumlah': jumlah
    }

    return render(request, 'template/rumahsakit.html', context)

def helpcenter(request):
    jenis_tempat = Rumahsakit.objects.filter(jenis = 'Help Center') #Kalo error ganti jadi two
    jumlah = jenis_tempat.count()
    
    context = {
        'data': jenis_tempat,
        'jenis': 'Help Center',
        'jumlah': jumlah
    }

    return render(request, 'template/rumahsakit.html', context)

def filterlokasi(request, id):
    if id == 1:  
        jenis_tempat = Rumahsakit.objects.filter(lokasi = 'Bandung')
        lokasi = "Bandung"
    elif id == 2:
        jenis_tempat = Rumahsakit.objects.filter(lokasi = 'Depok')
        lokasi = "Depok"
    elif id == 3:
        jenis_tempat = Rumahsakit.objects.filter(lokasi = 'Jakarta')
        lokasi = "Jakarta"
    elif id == 4:
        jenis_tempat = Rumahsakit.objects.filter(lokasi = 'Surabaya')
        lokasi = "Surabaya"
    else:
        jenis_tempat = Rumahsakit.objects.filter(lokasi = 'Tangerang')
        lokasi = "Tangerang"

    jumlah = jenis_tempat.count()
    
    context = {
        'data': jenis_tempat,
        'lokasi': lokasi,
        'jumlah': jumlah
    }

    return render(request, 'template/rumahsakit.html', context)


from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response

from rumahsakit.models import *
import rumahsakit.views 

class RumahsakitPageUnitTest(TestCase) :
    def test_url_is_exist (self) :
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)
    
    def test_buat_tempat(self) :
        rs = Rumahsakit.objects.create(jenis = "Rumah Sakit", nama = "RSUI", lokasi = "Tangerang", 
                                       waktu = "24 Jam", notelp = "0812391283", website = "https://www.rsui.com", 
                                       maps = "https://www.google.com/maps/place/Karang+Tengah+Medika+Hospital/@-6.210718,106.7029156,15z/data=!4m5!3m4!1s0x2e69f9e4d2428fdf:0x54773d92e399c617!8m2!3d-6.2100636!4d106.7150491", 
                                       link_foto = "https://asset.kompas.com/crops/4Yf9SwqnWtMpY6Ik8OI_UJ19v4Q=/0x0:0x0/750x500/data/photo/2019/11/29/5de0ada3ecb23.jpg")

        count = Rumahsakit.objects.all().count()
        self.assertEquals(count, 1)

    def test_template_render(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'base.html')
    
    def test_buat_lokasi(self):
        rs = Rumahsakit.objects.create(lokasi = "Jakarta")
        self.assertEquals(rs.lokasi, "Jakarta")

        rs= Rumahsakit.objects.create(lokasi = "Bandung")
        self.assertEquals(rs.lokasi, "Bandung")

        rs= Rumahsakit.objects.create(lokasi = "Surabaya")
        self.assertEquals(rs.lokasi, "Surabaya")
       
        rs= Rumahsakit.objects.create(lokasi = "Depok")
        self.assertEquals(rs.lokasi, "Depok")

        rs= Rumahsakit.objects.create(lokasi = "Tangerang")
        self.assertEquals(rs.lokasi, "Tangerang")

    def test_buat_jenis_tempat_hospital(self):
        response = Rumahsakit.objects.create(jenis = "Rumah Sakit")

        self.assertEquals(response.jenis, "Rumah Sakit")

    def test_buat_nama_rumahsakit(self):
        response = Rumahsakit.objects.create(nama = "rsui")
        self.assertEquals(str(response), "rsui")

    def test_buat_data_rumahsakit(self):
        response = Rumahsakit.objects.create(link_foto = "https://asset.kompas.com/crops/4Yf9SwqnWtMpY6Ik8OI_UJ19v4Q=/0x0:0x0/750x500/data/photo/2019/11/29/5de0ada3ecb23.jpg")
        
        count = Rumahsakit.objects.all().count()
        self.assertEquals(count, 1)

        response = Rumahsakit.objects.create(maps = "https://www.google.com/maps/place/Karang+Tengah+Medika+Hospital/@-6.210718,106.7029156,15z/data=!4m5!3m4!1s0x2e69f9e4d2428fdf:0x54773d92e399c617!8m2!3d-6.2100636!4d106.7150491")
        count = Rumahsakit.objects.all().count()
        self.assertEquals(count, 2)

        response = Rumahsakit.objects.create(website = "https://www.rsui.com")
        count = Rumahsakit.objects.all().count()
        self.assertEquals(count, 3)

    def test_hitung_filter_jenis_tempat(self):
        Rumahsakit.objects.create(jenis = "Rumah Sakit")
        count = Rumahsakit.objects.filter(jenis = "Rumah Sakit").count()
        self.assertEquals(count, 1)

    def test_hapus_data(self):
        Rumahsakit.objects.create(nama = "rsui")
        Rumahsakit.objects.get(id = 1).delete()

        count = Rumahsakit.objects.all().count()
        self.assertEquals(count, 0)

    def test_filter_jenis_hospital(self):
        Rumahsakit.objects.create(jenis = "Rumah Sakit")
        count = Rumahsakit.objects.filter(jenis = "Rumah Sakit").count()
        self.assertEquals(count, 1)

    def test_filter_jenis_helpcenter(self):
        Rumahsakit.objects.create(jenis = "Help Center")
        count = Rumahsakit.objects.filter(jenis = "Help Center").count()
        self.assertEquals(count, 1)


        




    

    

from django.urls import path

from . import views

app_name = 'rumahsakit'

urlpatterns = [
    path('', views.rumahsakit, name = 'rumahsakit'),
    path('tambah', views.tambah, name = 'tambah'),
    path('hospital', views.hospital, name = 'hospital'),
    path('helpcenter', views.helpcenter, name = 'helpcenter'),
    path('lokasi/<int:id>/', views.filterlokasi, name = 'filterlokasi')
]
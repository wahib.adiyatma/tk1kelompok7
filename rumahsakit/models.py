from django.db import models

class Rumahsakit(models.Model):
    choice = (
        ('Rumah Sakit', 'Rumah Sakit'),
        ('Help Center', 'Help Center')
    )

    lokasi = (
        ('Bandung', 'Bandung'),
        ('Depok', 'Depok'),
        ('Jakarta', 'Jakarta'),
        ('Surabaya', 'Surabaya'),
        ('Tangerang', 'Tangerang')
    )
    jenis = models.CharField(max_length=20, choices = choice, null = True )
    nama = models.CharField(max_length = 200, null = True)
    lokasi = models.CharField(max_length = 200, null = True, choices = lokasi)
    waktu = models.CharField(max_length = 200, null = True)
    notelp = models.CharField(max_length = 200, null = True)
    website = models.URLField(max_length = 200, null = True)
    maps = models.URLField(max_length = 100000, null = True)
    link_foto = models.URLField(max_length= 100000, null = True)
    def __str__(self):
        return self.nama 

